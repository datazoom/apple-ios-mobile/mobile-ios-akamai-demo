//
//  DemoAmpPlayerViewController.swift
//  AkamaiPlayerDemo
//
//  Created by Vinu Varghese on 17/04/20.
//  Copyright © 2020 DataZoom. All rights reserved.
//

import UIKit
import AmpCore
import AmpIMA
import AmpFreewheel
import GoogleInteractiveMediaAds
import DZAkamaiCollector
import DZAkamaiIMACollector
import DZAkamaiFreewheelCollector

class DemoAmpPlayerViewController: UIViewController {

    let kLicense = "AwF6vGZqECfLthNm7hNsbpeY79D+JEG1WxFoGZ6Wd5hUOb3Wv8cu1pjhmd58lAfqeqzwYT6m4m36ZhOAlYsWpYaOrSCTTVzb9QMe3Wo4blR5EtmaWovjXCGVj+OIJqvNJy6Y2wg7nUne+VDKtNoJ9DXyiH/FifGFJP8VEd9bsAremR2cSPWRly0lhyx7M7cvMryPiW+oaU2guD7dB04hbCTuTq8kLNdJYMQdnOqbdq3JCw=="

    // Use this for testing live sources
    let liveUrl = "https://liveprodeuwest.akamaized.net/eu1/Channel-EUTVqvs-AWS-ireland-1/Source-EUTVqvs-1000-1_live.m3u8"

    @IBOutlet weak var playerView: UIView!
    
    var player: AmpPlayer!
    var ima: AmpIMAManager!
    var buttonPushMe: UIButton!
    var backButton: UIButton!
    var preads = false
    var configId: String = ""
    var connectionURL: String = ""
    var playingUrl : Bool = false
    var adRequested = false
    var adsUrl = " "
    var preAdsUrl = " "
    var qocSet = false
    var ampFreewheel: AmpFreewheelManager!
    
    var selectedAddProvider : String?
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.adRequested = false
        // Do any additional setup after loading the view.
        if(preads){
            adsUrl = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator="
        }else{
             adsUrl = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpost&cmsid=496&vid=short_onecue&correlator="
        }
        self.qocSet = false
        player = AmpPlayer(parentView: self.playerView)
        ima = AmpIMAManager(ampPlayer: player, videoView: player.playerView!)
        let networkId = 42015
        let serverUrl = "http://demo.v.fwmrm.net"
        self.ampFreewheel = AmpFreewheelManager(ampPlayer: self.player, networkId: networkId)
        
        let videoAssetIdStr = "DZTEST\(playingUrl ? "LIVE" : "VOD")ASSET"
        let videoAssetId = VideoAssetId(id: videoAssetIdStr, duration: 0)
        
        player.title = "Demo title"
        player.logsEnabled = false
        player.autoplay = true
        player.setLicense(kLicense)
        player.registerObserver(self)
        
        if (playingUrl){
            player.play(url:liveUrl)
        }else{
        player.play(url: globalAssetValues)
        }
        
//        if let filePath = Bundle.main.path(forResource: "aotearoa", ofType: "mp4") {
//            print("File path \(filePath)")
//            let fileURL = URL(fileURLWithPath: filePath)
//            let urlAsset = AVURLAsset(url: fileURL)
//            player.play(asset: urlAsset)
//        }
        
        setUpPushMeButton()
        configurations()
        
        switch selectedAddProvider {
        case "IMA" :
            print("IMA")
            if !adRequested {
                print("AdsURL is ______\(adsUrl )")
                ima.requestImaAds(adTagUrl: adsUrl)
            }
        case "FreeWheel Ads" :
            print("FreeWheel Ads")
            self.ampFreewheel.requestAds(serverUrl: serverUrl, profile: "42015:ios_allinone_profile", siteSectionId: "ios_allinone_demo_site_section", videoAssetId: videoAssetId) {
                adController in
                adController.addPrerollSlot("superpre1")
                adController.addMidrollSlot("supermid1", timePosition: 30)
                adController.addPostrollSlot("postRoll1")
            }
            
        default:
            break
        }
        
        ima.registerImaObserver(self)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func configurations() {
        let gai = GAI.sharedInstance()
        gai?.tracker(withTrackingId: "UA-127776502-2")
        let tracker = gai?.defaultTracker
        var gaClientId = "";
        if (tracker != nil) {
            gaClientId = tracker!.get("&cid")!
        }
        print("GAClientId: \(gaClientId)")
        DZAmpCollector.dzSharedManager.flagForAutomationOnly = false
        DZAmpCollector.dzSharedManager.initAkamaiPlayerWith(configID: self.configId, url: self.connectionURL, playerInstance: self.player)
        print("DZ_SESSION_ID \(DZAmpCollector.dzSharedManager.getSessionId())")
        print("DZ_SESSION_VIEW_ID \(DZAmpCollector.dzSharedManager.getSessionViewId())")

        // Connecting with ampima
        DZAkamaiIMACollectorInstance.shared.connect(DZAmpCollector.dzSharedManager, ima)
    
        
        // Connecting with ampfreewheel
        DZAkamaiFreewheelCollectorInstance.shared.connect(DZAmpCollector.dzSharedManager)
        
        DZAmpCollector.dzSharedManager.customMetaData(["googleAnalyticsClientId": gaClientId])

        //MARK:- Custom Evnets And Metadata
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            let DZURL = NSURL(string: self.connectionURL)
            let domain = (DZURL?.host)!
            
            DZAmpCollector.dzSharedManager.customMetaData(["customPlayerName": "iOS Native Player", "customDomain": domain])
            DZAmpCollector.dzSharedManager.customEvents("SDKLoaded", metadata: nil)
    
            
        }
    }
        
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if self.buttonPushMe != nil {
            self.buttonPushMe.frame = CGRect(x: (self.view.frame.width / 2) - 45, y: 50, width: 90, height: 40)
        }
    }
    
    func setUpPushMeButton() {
        self.buttonPushMe = UIButton(frame: CGRect(x: (self.view.frame.width / 2) - 45, y: 50, width: 90, height: 40))
        self.buttonPushMe.layer.cornerRadius = 10.0
        self.buttonPushMe.setTitle("Push Me", for: .normal)
        self.buttonPushMe.setTitle("", for: .highlighted)
        self.buttonPushMe.setTitleColor(UIColor.white, for: .normal)
        self.buttonPushMe.backgroundColor = UIColor(red: 34 / 255, green: 218 / 255, blue: 34 / 255, alpha: 1.0)
        self.buttonPushMe.addTarget(self, action: #selector(self.buttonPushTouched), for: .touchUpInside)
        self.view.addSubview(self.buttonPushMe)
        self.view.bringSubview(toFront: self.buttonPushMe)

        self.backButton = UIButton(frame: CGRect(x: 30, y: 50, width: 50, height: 50))
        let image = UIImage(named: "back")
        self.backButton.setImage(image, for: .normal)
        self.backButton.setTitleColor(UIColor.white, for: .normal)
        self.backButton.layer.cornerRadius = 25.0
        self.backButton.addTarget(self, action: #selector(self.buttonBackTouched), for: .touchUpInside)
        self.view.addSubview(self.backButton)
        self.view.bringSubview(toFront: self.backButton)
    }

    @objc func buttonBackTouched() {
        self.player.stop()
        self.player.destroy()
        self.navigationController?.popViewController(animated: true)

    }

    @objc func buttonPushTouched() {
        print("PUSH ME")
        DZAmpCollector.dzSharedManager.customEvents("buttonClick", metadata: ["customPlayerName": "AkamaiPlayer", "customDomainName": "stagingdemo.datazoom.io"])
    }
}

extension DemoAmpPlayerViewController: PlayerEventObserver, IMAAdsManagerDelegate {

    func onBufferingStateChanged(_ ampPlayer: AmpPlayer) {
       
    }
    
    func adsManager(_ adsManager: IMAAdsManager!, didReceive event: IMAAdEvent!) {
        print("EVENT: \(event.typeString)")
    }
    
    func adsManager(_ adsManager: IMAAdsManager!, didReceive error: IMAAdError!) {
        
    }
    
    func onTimeChange(_ ampPlayer: AmpPlayer) {
        let duration = ampPlayer.playerItem?.currentTime().seconds ?? 0
        
        // Just mimicing the qoc request after 20 seconds of video
        
        if !self.qocSet {
            if duration > 20.0 {
                // This is the second on in the manifest
                let qos = QualityLevel(width: 640, height: 360, bitrate: 700)
                ampPlayer.setMaxQualityLevel(level: qos)
                self.qocSet = true
            }
        }
    }
    
    func adsManagerDidRequestContentPause(_ adsManager: IMAAdsManager!) {
        
    }
    
    func adsManagerDidRequestContentResume(_ adsManager: IMAAdsManager!) {
        
    }
    
    func adsManagerAdDidStartBuffering(_ adsManager: IMAAdsManager!) {
        print("BUFFER START")
    }
    
    func adsManagerAdPlaybackReady(_ adsManager: IMAAdsManager!) {
        print("BUFFER END")
    }

}
