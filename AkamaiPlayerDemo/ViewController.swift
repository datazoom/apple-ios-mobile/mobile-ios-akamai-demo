//
//  ViewController.swift
//  AkamaiPlayerDemo
//
//  Created by benedict placid on 26/02/19.
//  Copyright © 2019 benedict placid. All rights reserved.
//

import UIKit
import Foundation
import AmpCore


var globalAssetValues : String!

class ViewController: UIViewController, UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    
    
    
    
    
    @IBOutlet weak var videoOneEvent        : UIButton!
    @IBOutlet weak var fieldConfigId        : UITextView!
    @IBOutlet weak var envrmntSegment       : UISegmentedControl!
    @IBOutlet weak var connectingURLLabel   : UILabel!
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var selectedText: UITextField!
    var configId:String         = ""
    var connectionURL:String    = ""
    let devURL:String           = "https://devplatform.datazoom.io/beacon/v1/"
    let qaURL:String            = "https://stagingplatform.datazoom.io/beacon/v1/"
    let preProductionURL:String = "https://demoplatform.datazoom.io/beacon/v1/"
    let productionURL:String    = "https://platform.datazoom.io/beacon/v1/"
   // var list = ["VOD-No Ads","VOD- with Pre Roll Ads","VOD- with Pre Mid Post Roll Ads","LIVE- No Ads","LIVE-with Ads"]
    var list = ["VOD - No Ads","VOD - Pre Roll IMA","VOD - Pre Mid Post Roll IMA","LIVE - No Ads","LIVE - IMA","VOD - FREEWHEEL","LIVE - FREEWHEEL"]
    var StreamURL = false
    var adsPlay = false
    var preAds = false
    
    var selectedAddProvider : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TextPadding()
        self.fieldConfigId.text = "d79417de-c187-4771-8c1f-7a16906627b6"
        self.fieldConfigId.delegate = self
        self.configId = fieldConfigId.text
        self.connectingURLLabel.text = devURL
        self.connectionURL = devURL
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func validateConfigId(configIdL:String) -> Bool {
        if configIdL.range(of: "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", options: .regularExpression, range: nil, locale: nil) != nil {
            return true
        }else {
            return false
        }
    }
    func TextPadding(){
        selectedText.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: selectedText.frame.height))
        selectedText.leftViewMode = .always
    }
    
    @IBAction func videoOneEvent(_ sender: Any) {
        switch selectedText.text {
        case "VOD - No Ads":
            print(selectedText.text!)
            StreamURL = false
            adsPlay = true
            selectedAddProvider = "none"
        case "VOD - Pre Roll IMA":
            print(selectedText.text!)
            preAds = true
            StreamURL = false
            adsPlay = false
            selectedAddProvider = "IMA"
        case "VOD - Pre Mid Post Roll IMA":
            print(selectedText.text!)
            StreamURL = false
            adsPlay = false
            preAds = false
            selectedAddProvider = "IMA"
        case "LIVE - No Ads":
            print(selectedText.text!)
            StreamURL = true
            adsPlay = true
            selectedAddProvider = "none"
        case "LIVE - IMA" :
            preAds = true
            print(selectedText.text!)
            StreamURL = true
            adsPlay = false
            selectedAddProvider = "IMA"
        case "VOD - FREEWHEEL":
            preAds = true
            StreamURL = false
            adsPlay = false
            selectedAddProvider = "FreeWheel Ads"
        case "LIVE - FREEWHEEL" :
            preAds = true
            print(selectedText.text!)
           StreamURL = true
            adsPlay = false
            selectedAddProvider = "FreeWheel Ads"
        default:
            break
        }
      
            
        if validateConfigId(configIdL: self.configId) {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                    .instantiateViewController(withIdentifier: "DemoAmpPlayerViewController") as? DemoAmpPlayerViewController
            vc?.configId = self.configId
            vc?.connectionURL = self.connectionURL
            vc?.adRequested = self.adsPlay
            vc?.playingUrl = self.StreamURL
           vc?.preads = self.preAds
            
            vc?.selectedAddProvider = self.selectedAddProvider ?? "none"
            
            self.navigationController?.pushViewController(vc!, animated: true)
        }else {
            self.showInvalidConfigIdAlert()
            return
        }
        globalAssetValues = "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8"//https://thenewcode.com/assets/videos/atlantic-light.mp4"//"https://wowzaec2demo.streamlock.net/vod-multitrack/definst/smil:ElephantsDream/ElephantsDream.smil/playlist.m3u8"//"http://thenewcode.com/assets/videos/zsystems-alt.mp4"//"http://thenewcode.com/assets/videos/zsystems-alt.mp4"//"https://wowzaec2demo.streamlock.net/vod-multitrack/definst/smil:ElephantsDream/ElephantsDream.smil/playlist.m3u8"//"https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8"//"http://thenewcode.com/assets/videos/aotearoa.mp4"//"http://thenewcode.com/assets/videos/atlantic-light.mp4"//"https://thenewcode.com/assets/videos/atlantic-light.mp4"//"http://thenewcode.com/assets/videos/zsystems-alt.mp4"  //"httthenewcode.com/assets/videos/glacier.mp4"
    
    }
    
    func showInvalidConfigIdAlert() {
        let alert = UIAlertController(title: "", message: "Please enter a valid Configuration Id.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
  
    func textViewDidChange(_ textView: UITextView) {
        self.configId = fieldConfigId.text
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func didChangeEnvironment(_ sender: Any) {
        switch envrmntSegment.selectedSegmentIndex {
        case 0:
            self.connectionURL = self.devURL
            print("Dev selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        case 1:
            self.connectionURL = self.qaURL
            print("QA selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        case 2:
            self.connectionURL = self.preProductionURL
            print("Pre Productio selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        case 3:
            self.connectionURL = self.productionURL
            print("Productio selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return list.count
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{

    
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }

        label?.font = UIFont(name: "Helvetica Neue", size: 17)!
        label?.textColor = UIColor.white
        label?.text =  list[row] as? String
        label?.textAlignment = .center
        return label!

    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedText.text = self.list[row]
        
        self.pickerView.isHidden = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.selectedText{
            self.pickerView.isHidden = false
            textField.endEditing(true)
        }
    }
}




