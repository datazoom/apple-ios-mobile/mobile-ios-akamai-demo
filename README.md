# Akamai-Player-iOS-Demo
This repository contains demo app which are using the Akamai Player iOS SDK, and the DataZoom library to capture events from the Akamai player.

## Developer Instructions
### Prerequisites

Akamai-Player-iOS requires 

    • macOS Catalina Version: 10.15.2 or later
    • Xcode 11.1 with swift 5.1 version not more than or less than, because Amp Core is having 5.1 only
    • CocoaPods (https://guides.cocoapods.org/using/getting-started.html)

## Running the Akamai Demo app using Xcode:

* Clone the Project 
`git clone https://gitlab.com/datazoom/apple-ios-mobile/mobile-ios-akamai-demo.git`

* Open the project (Double click the project file with extension .xcworkspace)

* Select the required Simulator from top left in Xcode

* Click the Play button given on top left corner in Xcode to run the Project

* Add the following two lines in configuration method
	
    DZAkamaiCollector.dzSharedManager.flagForAutomationOnly = false

    DZAkamaiCollector.dzSharedManager.gaTrackerId = "Google Tracker Id"


![Screenshot pane](Xcode_ScreenShot.png)


* This will run the project and open the app in selected Simulator

* From simulator, the app can be tested by entering config id.

* You can choose between QA/Dev/Production environment. Make sure that you enter correct config id according to the environment selected.

![Screenshot pane](App_ScreenShot.png)

* You can observe the events generated and sent to connectors from the log in Xcode or you can check in corresponding connectors.


## Dependencies

* Make sure that you use Xcode Version 11.1  

* If you want to use the Framework alone, You can download it from **[Here](https://gitlab.com/datazoom/apple-ios-mobile/mobile-ios-collector-frameworks/tree/master/akamai-collector)**.

* For Instructions on how to use the Framework, You can refer **[Here](https://datazoom.atlassian.net/wiki/spaces/EN/pages/322602372/Akamai+Player+for+iOS)** .   

## Credit

- Vishnu M P

## Link to License/Confidentiality Agreement
Datazoom, Inc ("COMPANY") CONFIDENTIAL
Copyright (c) 2017-2018 [Datazoom, Inc.], All Rights Reserved.
NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
Confidentiality and Non-disclosure agreements explicitly covering such access.
The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.



## Using The Akamai Player iOS SDK
When you want to develop an own iOS application using the Akamai Player iOS SDK read through the following steps.

### Adding the SDK To Your Project
To add the SDK as a dependency to your project, you have two options: Using CocoaPods or adding the SDK bundle directly.


#### Using CocoaPods
Add `pod 'AMPCore'` to your Podfile. After that, install the pod using `pod install`. See the `Podfile` of this repository for a full example.

#### Adding the SDK Directly
+   When using XCode, go to the `General` settings page and add the SDK bundle (`AMPCore.framework`) under `Linked Frameworks and Libraries`. The SDK bundle can be downloaded from the [release page of the GitHub repository](https://github.com/Akamai/Akamai-player-ios-sdk-cocoapod/releases).





