// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.1.3 (swiftlang-1100.0.282.1 clang-1100.0.33.15)
// swift-module-flags: -target x86_64-apple-ios10.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name DataZoom
import AdSupport
import CoreFoundation
import CoreTelephony
@_exported import DataZoom
import Foundation
import Security
import Swift
import SystemConfiguration
import UIKit
@objc open class DZNativeConnection : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc deinit
}
public class DZEventCollector {
  public static var dzEventCollectorManager: DataZoom.DZEventCollector
  public var name: Swift.String
  public var version: Swift.String
  public var connectionURL: Swift.String
  public var dzSharedCombinedConnector: Swift.String
  public var sessionViewID: Swift.String
  public var oAuthToken: Swift.String
  public var customerCode: Swift.String
  public var connectorList: Swift.String
  public var configurationId: Swift.String
  public var os: Swift.String
  public var deviceName: Swift.String
  public var systemName: Swift.String
  public var systemModel: Swift.String
  public var systemVersion: Swift.String
  public var internetServiceProvider: Swift.String
  public var orientation: Swift.String
  public var asset_id: Swift.String
  public var user_agent: Swift.String
  public var player_state: Swift.String
  public var player_volume: Swift.Int
  public var player_bitRate: Swift.Float
  public var playback_rate: Swift.Float
  public var play_head_position: Swift.Float
  public var player_item_duration: Swift.Float
  public var player_buffer_fill: Swift.Float
  public var toMilliss: Swift.Float64
  public var fromMilliss: Swift.Float64
  public var eventList: [Swift.String]
  public var isMuted: Swift.Bool
  public var autoStart: Swift.Bool
  public var controls: Swift.Bool
  public var defaultPlaybackRate: Swift.Float
  public var loop: Swift.Bool
  public var fullscreen: Swift.Bool
  public var readyState: Swift.String
  public var streamingProtocol: Swift.String
  public var streamingType: Swift.String
  public var numberOfVideos: Swift.Int
  public var numberOfErrors: Swift.Int
  public var defaultMuted: Swift.Bool
  public var timeSinceBufferBegin: Swift.Int
  public var timeSinceLastFluxData: Swift.Int
  public var timeSincePaused: Swift.Int
  public var timeSinceRequested: Swift.Int
  public var timeSinceSeekBegin: Swift.Int
  public var timeSinceStarted: Swift.Int
  public var totalPlayTime: Swift.Float
  public var renditionName: Swift.String
  public var renditionHeight: Swift.Int
  public var renditionWidth: Swift.Int
  public var renditionBitrate: Swift.Int
  public var absShift: Swift.String
  public var unsentJsonObjectArray: [[Swift.String : Any]]
  public var asn: Swift.String
  public var city: Swift.String
  public var country: Swift.String
  public var countryCode: Swift.String
  public var isp: Swift.String
  public var latitude: Swift.Double
  public var longtitude: Swift.Double
  public var org: Swift.String
  public var query: Swift.String
  public var region: Swift.String
  public var regionName: Swift.String
  public var zip: Swift.String
  public var connectionType: Swift.String
  public var customMetadata: [Swift.String : Any]
  public var retryJsonObjectArray: [[Swift.String : Any]]
  public var resolutionHeight: Swift.Int
  public var resolutionWidth: Swift.Int
  public var title: Swift.String
  public var flagForAutomationOnly: Swift.Bool
  open func triggerMessage(eventOccured: Swift.String, rateValue: Swift.Float, fromValue: Swift.Float64, toValue: Swift.Float64, onCompletion: @escaping (Swift.String) -> Swift.Void)
  @objc deinit
}
public class SSLCert {
  public init(data: Foundation.Data)
  public init(key: Security.SecKey)
  @objc deinit
}
public class SSLSecurity {
  public var validatedDN: Swift.Bool
  public convenience init(usePublicKeys: Swift.Bool = false)
  public init(certs: [DataZoom.SSLCert], usePublicKeys: Swift.Bool)
  public func isValid(_ trust: Security.SecTrust, domain: Swift.String?) -> Swift.Bool
  @objc deinit
}
public enum ReachabilityError : Swift.Error {
  case FailedToCreateWithAddress(Darwin.sockaddr_in)
  case FailedToCreateWithHostname(Swift.String)
  case UnableToSetCallback
  case UnableToSetDispatchQueue
}
@available(*, unavailable, renamed: "Notification.Name.reachabilityChanged")
public var ReachabilityChangedNotification: Foundation.NSNotification.Name
extension NSNotification.Name {
  public static var reachabilityChanged: Foundation.Notification.Name
  public static var connectionEstablished: Foundation.Notification.Name
}
public class Reachability {
  public typealias NetworkReachable = (DataZoom.Reachability) -> ()
  public typealias NetworkUnreachable = (DataZoom.Reachability) -> ()
  @available(*, unavailable, renamed: "Connection")
  public enum NetworkStatus : Swift.CustomStringConvertible {
    case notReachable
    case reachableViaWiFi
    case reachableViaWWAN
    public var description: Swift.String {
      get
    }
    public static func == (a: DataZoom.Reachability.NetworkStatus, b: DataZoom.Reachability.NetworkStatus) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  public enum Connection : Swift.CustomStringConvertible {
    case none
    case wifi
    case cellular
    public var description: Swift.String {
      get
    }
    public static func == (a: DataZoom.Reachability.Connection, b: DataZoom.Reachability.Connection) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  public var whenReachable: DataZoom.Reachability.NetworkReachable?
  public var whenUnreachable: DataZoom.Reachability.NetworkUnreachable?
  @available(*, deprecated, renamed: "allowsCellularConnection")
  final public let reachableOnWWAN: Swift.Bool
  public var allowsCellularConnection: Swift.Bool
  public var notificationCenter: Foundation.NotificationCenter
  @available(*, deprecated, renamed: "connection.description")
  public var currentReachabilityString: Swift.String {
    get
  }
  @available(*, unavailable, renamed: "connection")
  public var currentReachabilityStatus: DataZoom.Reachability.Connection {
    get
  }
  public var connection: DataZoom.Reachability.Connection {
    get
  }
  required public init(reachabilityRef: SystemConfiguration.SCNetworkReachability, usingHostname: Swift.Bool = false)
  public convenience init?(hostname: Swift.String)
  public convenience init?()
  @objc deinit
}
extension Reachability {
  public func startNotifier() throws
  public func stopNotifier()
  @available(*, deprecated, message: "Please use `connection != .none`")
  public var isReachable: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "Please use `connection == .cellular`")
  public var isReachableViaWWAN: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "Please use `connection == .wifi`")
  public var isReachableViaWiFi: Swift.Bool {
    get
  }
  public var description: Swift.String {
    get
  }
}
public var ssessionIDFlag: Swift.Bool
@objc public class DZWebBroker : ObjectiveC.NSObject, DataZoom.WebSocketDelegate {
  public static var dzwBkr: DataZoom.DZWebBroker
  public var eventList: [Swift.String]
  public var metaDataList: [Swift.String]
  public var fluxDataList: [Swift.String]
  public var interval: Swift.Int
  public var asn: Swift.String
  public var city: Swift.String
  public var country: Swift.String
  public var countryCode: Swift.String
  public var isp: Swift.String
  public var latitude: Swift.Double
  public var longtitude: Swift.Double
  public var org: Swift.String
  public var query: Swift.String
  public var region: Swift.String
  public var regionName: Swift.String
  public var zip: Swift.String
  public func websocketDidConnect(_ socket: DataZoom.DZWebSocket)
  public func websocketDidDisconnect(_ socket: DataZoom.DZWebSocket, error: Foundation.NSError?)
  public func websocketDidReceiveMessage(_ socket: DataZoom.DZWebSocket, text: Swift.String)
  public func websocketDidReceiveData(_ socket: DataZoom.DZWebSocket, data: Foundation.Data)
  @objc override dynamic public init()
  @objc deinit
}
@objc public enum DZConfigState : Swift.Int {
  case COLLECTOR_CONNECTED
  case COLLECTOR_NOT_CONNECTED
  case CONFIG_INIT_ERROR
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
public var sourceURL: Swift.String
public var dzSessionIdentifier: Swift.String
@objc open class DZConnector : ObjectiveC.NSObject {
  public static var dzConnector: DataZoom.DZConnector
  public var configId: Swift.String
  public var gaClientId: Swift.String
  public var presentTime: Swift.Float64
  public var prevTime: Swift.Float64
  public var userAgent: Swift.String
  public var timer: Foundation.Timer?
  public var stopped: Swift.Bool
  final public let reachability: DataZoom.Reachability
  public var timeObserverToken: Swift.AnyObject?
  public var periodicObserverToken: Swift.AnyObject?
  public var flagForAutomationOnly: Swift.Bool
  @objc open func initialise(withConfigId: Swift.String, andURL: Swift.String, withGAClientID: Swift.String, userAgent: Swift.String, onCompletion: @escaping (DataZoom.DZConfigState, Swift.Error?) -> Swift.Void)
  public func getSessionID() -> Swift.String
  @objc open func generateSessionID() -> Swift.String?
  @objc override dynamic public init()
  @objc deinit
}
extension UIDevice {
  public var modelName: Swift.String {
    get
  }
}
public var WebsocketDidConnectNotification: Swift.String
public var WebsocketDidDisconnectNotification: Swift.String
public var WebsocketDisconnectionErrorKeyName: Swift.String
public protocol WebSocketDelegate : AnyObject {
  func websocketDidConnect(_ socket: DataZoom.DZWebSocket)
  func websocketDidDisconnect(_ socket: DataZoom.DZWebSocket, error: Foundation.NSError?)
  func websocketDidReceiveMessage(_ socket: DataZoom.DZWebSocket, text: Swift.String)
  func websocketDidReceiveData(_ socket: DataZoom.DZWebSocket, data: Foundation.Data)
}
public protocol WebSocketPongDelegate : AnyObject {
  func websocketDidReceivePong(_ socket: DataZoom.DZWebSocket)
}
@objc public class DZWebSocket : ObjectiveC.NSObject, Foundation.StreamDelegate {
  public enum CloseCode : Swift.UInt16 {
    case normal
    case goingAway
    case protocolError
    case protocolUnhandledType
    case noStatusReceived
    case encoding
    case policyViolated
    case messageTooBig
    public typealias RawValue = Swift.UInt16
    public init?(rawValue: Swift.UInt16)
    public var rawValue: Swift.UInt16 {
      get
    }
  }
  public static var ErrorDomain: Swift.String
  public var callbackQueue: Dispatch.DispatchQueue
  weak public var delegate: DataZoom.WebSocketDelegate?
  weak public var pongDelegate: DataZoom.WebSocketPongDelegate?
  public var onConnect: (() -> Swift.Void)?
  public var onDisconnect: ((Foundation.NSError?) -> Swift.Void)?
  public var onText: ((Swift.String) -> Swift.Void)?
  public var onData: ((Foundation.Data) -> Swift.Void)?
  public var onPong: (() -> Swift.Void)?
  public var headers: [Swift.String : Swift.String]
  public var voipEnabled: Swift.Bool
  public var selfSignedSSL: Swift.Bool
  public var security: DataZoom.SSLSecurity?
  public var enabledSSLCipherSuites: [Security.SSLCipherSuite]?
  public var origin: Swift.String?
  public var timeout: Swift.Int
  public var isConnected: Swift.Bool {
    get
  }
  public var currentURL: Foundation.URL {
    get
  }
  public init(url: Foundation.URL, protocols: [Swift.String]? = nil)
  public func connect()
  public func disconnect(forceTimeout: Foundation.TimeInterval? = nil)
  public func write(string: Swift.String, completion: (() -> ())? = nil)
  public func write(data: Foundation.Data, completion: (() -> ())? = nil)
  public func write(_ ping: Foundation.Data, completion: (() -> ())? = nil)
  @objc public func stream(_ aStream: Foundation.Stream, handle eventCode: Foundation.Stream.Event)
  @objc deinit
  @objc override dynamic public init()
}
open class DZUtils {
  public static func getSessionID() -> Swift.String
  @objc deinit
}
